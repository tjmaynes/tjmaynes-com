+++
title = "About"
description = "My name is TJ Maynes and I'm a passionate, generalist Software Engineer at VMware Tanzu Labs. Also, I'm writing screenplays and making music."
template = "content.html"
+++
![maine, emmy and me](/assets/images/me.jpg)

My name is TJ Maynes and I'm a passionate, generalist Software Engineer at [VMware Tanzu Labs](https://tanzu.vmware.com/labs). Over my career I've been fortunate to work with amazing people at some pretty incredible places.

## [VMware Tanzu Labs](https://tanzu.vmware.com/labs)

September 15th, 2019 was my first day as a Consultant at Pivotal Labs. Since then, we've since been rebranded as VMware Tanzu Labs and I've worked on 12+ software enablement and delivery engagements. I've pushed Swift/SwiftUI, Spring, .NET Core, Typescript/React, Kubernetes YAMLs and Bash to production environments.

VMware Tanzu Labs is a fantastic place to work and if your interested in working here, I highly recommend checking out the [careers site](https://careers.vmware.com/main/jobs?keywords=labs&location=New%20York).

## NBCUniversal

On December 1st, 2016, I started my first day on a newly formed product team within the Content Distribution department at NBCUniversal. At NBCUniversal, our team was responsible for managing and adding new features to the MSNBC, Eonline, Syfy, Oxygen, Telemundo, USA, and Universo. Much of my time at NBCUniversal was spent working on backend services, (sole developer for) one website for the World Cup and automating our AWS infrastructure.

I had an amazing time with some really talent people and great friends whom have influenced my career and my life.

## [Rejuvenan](https://www.rejuvenan.com/)

May of 2015 was a busy month for me. In the span of a month:
1. I graduated from the [University of South Florida](https://www.usf.edu/engineering/cse/)
2. I accepted an offer to work at Rejuvenan Global Health.
3. I moved from Florida to New Jersey with my future fiancée, [Emily Baron](https://embryoh.com/).

Rejuvenan is a stealth health-tech startup in Mid-town Manhattan that specialized in creating a health plan for customers based on their blood tests and telomere science. At Rejuvenan I was the sole iOS developer of the Rejuvenan iOS app. I worked closely with fullstack Ruby on Rails team whom influenced my first foray into test-driven development and pair programming.

Having my first full-time job at Rejuvenan was a gift and I'll never forget the team that took a chance on me.

## [Spatial Networks](https://www.fulcrumapp.com/)

While studying for my Computer Science degree, I accepted a Web Developer Internship at Spatial Networks, a company developing an iOS and Android app for geospatial data collection called [Fulcrum](https://www.fulcrumapp.com/company). At Spatial Networks, I learned how to contribute to a Ruby on Rails codebase with a small, yet mighty, team of developers.

If you have any questions, feel free to reach out to [me](mailto:tj@tjmaynes.com)!

You can find out more about me in my [now page](/now) and [here](/links).
