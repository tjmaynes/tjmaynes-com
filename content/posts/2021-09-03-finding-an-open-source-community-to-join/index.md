+++
title = "Finding an Open Source Community to join"
description = "" 
date = "2021-09-03 13:55:06"
draft = true

[extra]
author = "tjmaynes"
+++
*TL;DR*: To begin your search for an open source community to join first ask yourself what you'd like to spend more time doing. Maybe that's data science, iOS development, Kubernetes or some other interesting technology stack or vertical. Next, do some research to find out where your desired community communicates the most. Maybe that's Twitter, Reddit, Slack, or Discord. Finally, join the open source community and start a conversation!

# Introduction

## What are your interests?

## Find the 