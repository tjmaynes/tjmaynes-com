+++
title = "Why Emacs"
description = "This blog post is an unordered list of reasons why I enjoy using Emacs."
slug = "why-emacs"
date = "2014-07-25 12:00:00"

[taxonomies]
tags=["personal"]

[extra]
author = "tjmaynes"
+++
![my emacs](emacs.jpg)

I've been using Emacs roughly a year now and I just wanted to write a blog post to discuss how awesome Emacs is and things that I enjoy about Emacs.

## [Elisp](https://en.wikipedia.org/wiki/Emacs_Lisp)
Emacs uses a dialect of Lisp called `elisp`, or Emacs Lisp, which is quite powerful for making small (or large) scripts to customize emacs working environment. Also, in my opinion its important as a developer to learn a little Lisp and understand just how incredible the language is/can be. It's quite beautiful that a text editor can be configured to what you want/desire by using a language that many programmers are familiar with.

## [Org-mode](http://orgmode.org/)
Org-mode is my favorite way to get work completed and also efficient enough to record some thoughts from my mind. I've used Evernote and then switched to OneNote (which was an excellent experience). I use org-mode for jotting down things I learn from the internet, my weekly schedule, writing small stories, keeping my notes organized, etc. I also use git to sync my org files with a remote server.

## [EMMS](https://www.gnu.org/software/emms/)
EMMS stands for *Emacs MultiMedia System* which I use primarily for internet radio and listening to local (or remote) music directories. I can't say much more about EMMS than that its really neat that my text editor allows me to play music (so no need for spotify, itunes, etc).

## [Doc-view mode](https://www.gnu.org/software/emacs/manual/html_node/emacs/Document-View.html)
I have found doc-view-mode to be a life saver from constantly having to look back and forth between code and text. Although my only complaint is that I can't really use doc-view-mode for large pdf files (>10mb), which will cause Emacs to hang and *possibly* crash.

## [M-x package-list-packages](https://www.gnu.org/software/emacs/manual/html_node/emacs/Package-Menu.html)
I think a real draw of emacs is having the ability to download emacs packages (emms, cider/slime, org-mode, python-mode, etc) from within emacs. Also, all the packages I use are found (organized) in my emacs.d folder in my home directory, which is nice since I dont have to search very far if I need to modify or manually delete a package.

# Conclusion
Although, Emacs did take a little bit to get used to, it ultimately paid off in the long run. I don't want to end this post thinking that I purely only use emacs (especially since I use vim when I'm working from a remote server). Emacs is highly extensible (and free) and I love it.

## Links
* [Emacs quick tips](http://zoo.cs.yale.edu/classes/cs210/help/emacs.html)
* [Famous people that use emacs](http://wenshanren.org/?p=418)
* [Emacs rocks!](http://emacsrocks.com/)
