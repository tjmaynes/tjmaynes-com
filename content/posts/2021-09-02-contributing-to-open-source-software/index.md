+++
title = "Contributing to Open Source Software"
description = "If you are having a hard time finding ways to contribute to open software software, you can take fix warnings in third-party libraries, update third-party library documentation, tweeting about your discoveries and/or creating a small sandbox project."
date = "2021-09-02 14:53:10"
draft = true

[extra]
author = "tjmaynes"
+++
**TL;DR**: If you are having a hard time finding ways to contribute to open software software, you can take fix warnings in third-party libraries, update third-party library documentation, tweeting about your discoveries and/or creating a small sandbox project.

# Introduction

I wish that I had contributed more often to open source projects earlier on in my career. I felt pretty insecure over my software skills back then. What can I contribute? Why does my voice matter in a sea of *incredibly smart* engineers? If you're feeling like I did, **I want you to know** that your voice, your ideas and your empathy are all wanted within healthy open source communities and you shouldn't be afraid to use them. I wish I knew this sooner back then, but I'm here to tell you now that if you're *even* thinking about contributing to open source software, you're heart and ideas are in the right place. In this blog post, I'm going to go over a list of small, yet mighty, ways to contribute to open source software without breaking the time, energy and emotion bank.

## Write a blog post on your learnings



## Building a sandbox project

## Fixing open project issues

## Fixing project warnings

## Updating documentation / <-- user onboarding

# Conclusion

Keep it small, keep it simple, the bigger it gets the hard to publish or deploy.