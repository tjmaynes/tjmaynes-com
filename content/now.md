+++
title = "Now"
description = "This is my Now page, inspired by Derek Sivers' \"Now Now Now\" movement."
template = "content.html"
+++
# What I’m up to right now 
*💡 This is my [Now page](https://nownownow.com/about), inspired by Derek Sivers' [Now Now Now](https://nownownow.com/) movement.*

## 👨‍💻  What I'm doing
- Engineering at [VMware Tanzu Labs](https://tanzu.vmware.com/labs) - software enablement and delivery consulting on client projects. Usually *test-driving and pairing* on the following projects:
    - Mobile apps (iOS/Android)
    - React web apps (Typescript/Javascript)
    - Backend services (.NET Core/Spring)
- Going deeper into [React](https://reactjs.org) and [Go](https://www.go.dev/).
- Playing making movies and music.

## 🌍  Where am I
At the moment, I'm living in Rochester, Minnesota.

*You can learn more from my [CV](/career/cv.pdf).*
